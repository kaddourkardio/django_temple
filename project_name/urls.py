from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin


admin.autodiscover()
urlpatterns = []

if settings.DEBUG:
    import debug_toolbar


    urlpatterns += [url(r'^__debug__/', include(debug_toolbar.urls))]
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    # urlpatterns += [
    #     url(r'^404/', views.custom_404),
    # ]

urlpatterns += [
    url(r'^admin/', include(admin.site.urls)),
]
